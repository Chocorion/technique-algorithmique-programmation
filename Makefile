CC = gcc
CFLAGS = -Wall -g -Wno-unused-function -Wno-deprecated-declarations
LDLIBS = -lm -lglut -lGLU -lGL -lSDL2 -lGLU -lGL

tsp: tsp_main.c tools.c my_draw.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

test_heap: test_heap.c heap.c
	$(CC) $(CFLAGS) $^ -o $@

a_star: a_star.c tools.c heap.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

clean:
	rm -f tsp
	rm -f test_heap
	rm -f a_star
	rm -fr *.dSYM/
