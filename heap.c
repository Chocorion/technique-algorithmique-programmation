#include "heap.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**
 * Swap two variables of type void *
 **/
void swap(void** objet_1, void** objet_2) {
	void *tmp = *objet_1;
	*objet_1 = *objet_2;
	*objet_2 = tmp;
}

heap heap_create(int n, int (*f)(const void *, const void *)) {
	heap h = malloc(sizeof(*h));

	h->nmax = n;
	h->size = 0;
	h->f = f;

	//Index 1 to n plus 0
	h->array = malloc((n + 1) * sizeof(void *));
  
  	return h;
}


void heap_destroy(heap h) {
	if (!h) {
		return;
	}

	free(h->array);
	free(h);
}

int heap_empty(heap h) {
  	return h->size == 0;
}

int heap_add(heap h, void *object) {
	//If the heap is full
	if (h->size + 1 > h->nmax) {
		return 1;
	}

	//The first free place in the array
	int index = h->size + 1;
	
	int father_index;
	father_index = floor(index/2);

	h->array[index] = object;

	while (father_index > 0 && father_index != index && 
		(h->f(h->array[father_index], h->array[index]) > 0)
	) {
		swap(&h->array[father_index], &h->array[index]);

		index = father_index;
		father_index = floor(father_index/2);
	}

	h->size++;
  	return 0;
}

void *heap_top(heap h) {
	if (heap_empty(h)) {
		return NULL;
	}

	return h->array[1]; 
}

/**
 * Return the index of the minimal child of node index, or -1.
 */
int minimum_son_index(heap h, int index) {
	if ((index * 2) + 1 > h->size) {
		if (index * 2 > h->size) {
			return -1;
		}
		
		//Can has only one child.
		return index * 2; 
	}

	if (h->f(h->array[index * 2], h->array[(index * 2) + 1]) < 0) {
		return index * 2;
	}

	return index * 2 + 1;
}

void *heap_pop(heap h) {
	if (heap_empty(h)) {
		return NULL;
	}

	void* object = heap_top(h);
	int index = 1;
	
	//Put the last object in the root position
	h->array[index] = h->array[h->size];
	int son_index = minimum_son_index(h, index);

	while(index <= h->nmax && son_index > 0 && h->f(h->array[index],h->array[son_index]) > 0) {
		swap(&h->array[index], &h->array[son_index]);

		index = son_index;
		son_index = minimum_son_index(h, index);
	}

	h->size--;
	return object;
}
