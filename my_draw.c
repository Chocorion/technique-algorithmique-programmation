#include "my_draw.h"



const int WIDTH   = 640;
const int HEIGHT  = 480;

SDL_Window    *Window = NULL;
SDL_Renderer  *Render = NULL; 


void error_sdl() {
    fprintf(stderr, "Erreur sdl: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
}

void init_sdl() {
    if (SDL_Init(SDL_INIT_VIDEO)) error_sdl();

    Window = SDL_CreateWindow(
        "Voyageur de commerce Avec des jolies lignes",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        WIDTH,
        HEIGHT,
        SDL_WINDOW_SHOWN
    );
    if (!Window) error_sdl();

    Render = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED);
    if (!Render) error_sdl();
}

void draw_single_line_sdl(point a, point b) {
    if (SDL_RenderDrawLine(
        Render, 
        (int) a.x,
        (int) a.y,
        (int) b.x,
        (int) b.y)) {
            error_sdl();
        }
}
void draw_single_point_sdl(point a) {
    if (SDL_RenderDrawPoint(
        Render, 
        (int) a.x,
        (int) a.y
        )) {
            error_sdl();
        }
}

void draw_path_sdl(point *V, int n, int *P, int k) {
    SDL_SetRenderDrawColor(Render, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(Render);

    SDL_SetRenderDrawColor(Render, 255, 255, 255, SDL_ALPHA_OPAQUE);
    for (int i = 0; i < k; i++) {
        draw_single_line_sdl(V[P[i]], V[P[(i + 1)%k]]);
    }

    SDL_SetRenderDrawColor(Render, 255, 0, 0, SDL_ALPHA_OPAQUE);
    for (int i = 0; i < n; i++) {
        draw_single_point_sdl(V[i]);
    }

    SDL_RenderPresent(Render);
}