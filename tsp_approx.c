//
//  TSP - APPROXIMATION / HEURISTIQUES
//

#include "tools.h"

static double d(point A, point B)
{
	double x = B.x - A.x;
	double y = B.y - A.y;

	return sqrt(x * x + y * y);
}

static double value(point *V, int n, int *P){
	double s = 0;

	for (int i = 0; i < n - 1; i++) {
		s += d(V[P[i]], V[P[i + 1]]);
	}

	s += d(V[P[n - 1]], V[P[0]]);

	return s;
}

void reverse(int *T, int p, int q) {
	int tmp;
	while(p < q) {
		tmp = T[p];
		T[p] = T[q];
		T[q] = tmp;

		p++;
		q--;
	}
	return;
}

double first_flip(point *V, int n, int *P, int used_static) {
	static int where = 0;
	static int reset = 1;
	if (!used_static && reset) {
		where = 0;
		reset = 0;
	}
	double edge_1, current_edge, new_edge;

	for (int i = where; i < n - 2; i++) {//where pour le fun. 10 000 en 30secondes sur mon petit i5
		edge_1 = d(V[P[i]], V[P[i+1]]);

		for (int j = i+2; j < n; j++) {
			current_edge = d(V[P[j]], V[P[(j+1)%n]]);
			new_edge = d(V[P[i]], V[P[j]]) + d(V[P[i+1]], V[P[(j+1)%n]]);

			if (new_edge < edge_1 + current_edge) {
				reverse(P, i+1, j);
				where = i;
				return edge_1 + current_edge - new_edge;
			}
		}
	}

	return 0.0;
	
}

double tsp_flip(point *V, int n, int *P) {
	for (int i = 0; i < n; i++) P[i] = i;
	while(first_flip(V, n, P, 1)) {
		drawTour(V, n, P);
	}
	while (first_flip(V, n, P, 0))
	{
		drawTour(V, n, P);
	}
	return value(V, n, P);
}


double tsp_greedy(point *V, int n, int *P) {

	for (int i = 0; i < n; i++) {
		P[i] = i;
	}

	double dist_current;
	double dist_min = -1;
	int index_min, tmp;

	for (int i = 0; i < n - 1; i++) {
		dist_min = DBL_MAX;

		for (int k = i + 1; k < n; k++) {

			dist_current = d(V[P[i]], V[P[k]]);

			if (dist_current < dist_min) {
				dist_min = dist_current;
				index_min = k;
				drawTour(V, n, P);
			}
		}

		tmp = P[i+1];
		P[i+1] = P[index_min];
		P[index_min] = tmp;
		
	}
	//Retour
	P[n] = 0;
	drawTour(V, n, P);

	return value(V, n, P);
}
