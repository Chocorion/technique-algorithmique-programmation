#ifndef MY_DRAW_H
#define MY_DRAW_H

#include "tools.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>

void error_sdl();
void init_sdl();
void draw_single_line_sdl(point a, point b);
void draw_single_point_sdl(point a);
void draw_path_sdl(point *V, int n, int *P, int k);

#endif