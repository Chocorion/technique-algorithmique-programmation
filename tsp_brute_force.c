#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "tools.h"

static double dist(point A, point B)
{
  double x = B.x - A.x;
  double y = B.y - A.y;

  return x * x + y * y;
}

static double value(point *V, int n, int *P);

static double tsp_brute_force(point *V, int n, int *Q)
{
  int tmp[n];

  for (int i = 0; i < n; i++)
    tmp[i] = i;
  double v, min = value(V, n, tmp);

  while (NextPermutation(tmp, n))
  {
    //drawTour(V, n, tmp);
    if ((v = value(V, n, tmp)) < min)
    {
      min = v;
      for (int i = 0; i < n; i++)
        Q[i] = tmp[i];
    }
  }
  return min;
}

static void MaxPermutation(int *P, int n, int k)
{
  int tmp;
  for (int i = k; i < n; i++)
  {
    tmp = P[i];
    for (int j = i + 1; j < n; j++)
    {
      if (P[j] > P[i])
      {
        tmp = P[j];
        P[j] = P[i];
        P[i] = tmp;
      }
    }
  }
}

static double value_opt(point *V, int n, int *P, double w)
{
  double s = 0;
  int k = 0;
  for (; k < n && s < w; k++)
    s += dist(V[P[k]], V[P[(k + 1) % n]]);
  return (s < w) ? s : -(k + 1);
}

static double tsp_brute_force_opt(point *V, int n, int *Q)
{
  int tmp[n];

  for (int i = 0; i < n; i++)
    tmp[i] = i;
  double v, min = value(V, n, tmp);

  while (NextPermutation(tmp, n))
  {
    v = value_opt(V, n, tmp, min);
    if (v < 0)
    {
      MaxPermutation(tmp, n, -v);
      continue;
    }
    if (v < min)
    {
      min = v;
      for (int i = 0; i < n; i++)
        Q[i] = tmp[i];
    }
  }
  return min;
}

